<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="nb_NO" sourcelanguage="en">
<context>
    <name>MainWindow</name>
    <message encoding="UTF-8">
        <location filename="pling-plong-odometer.ui" line="18"/>
        <source>♫ ♪ Odometer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="31"/>
        <source>Project</source>
        <translation>Prosjekt</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="37"/>
        <source>Production no.</source>
        <translation>Prodnummer</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="59"/>
        <source>Drop a Final Cut Pro sequence (XML export) anywhere to load it.</source>
        <translation>Slipp en Final Cut Pro-sekvens (XML-eksport) på et vilkårlig sted for å laste den inn.</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pling-plong-odometer.ui" line="90"/>
        <source>♫</source>
        <translation></translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="98"/>
        <source>Clip name</source>
        <translation>Klippnavn</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="103"/>
        <source>Duration</source>
        <translation>Varighet</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="111"/>
        <source>Metadata</source>
        <translation>Metadata</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="122"/>
        <source>Errors and warnings</source>
        <translation>Feil og advarsler</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="144"/>
        <source>Details</source>
        <translation>Detaljer</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="303"/>
        <source>Close the Details box</source>
        <translation>Lukk Detaljer</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="320"/>
        <source>Locate a Final Cut Pro sequence (XML export) and load it.</source>
        <translation>Finn en Final Cut Pro-sekvens (XML-eksport) og last den inn.</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="323"/>
        <source>&amp;Open file...</source>
        <translation>Å&amp;pne fil...</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="150"/>
        <source>&lt;b&gt;Title&lt;/b&gt;</source>
        <translation>&lt;b&gt;Tittel&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="167"/>
        <source>&lt;b&gt;Artist&lt;/b&gt;</source>
        <translation>&lt;b&gt;Artist&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="201"/>
        <source>&lt;b&gt;Composer&lt;/b&gt;</source>
        <translation>&lt;b&gt;Komponist&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="252"/>
        <source>&lt;b&gt;Album&lt;/b&gt;</source>
        <translation>&lt;b&gt;Album&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="286"/>
        <source>&lt;b&gt;Year&lt;/b&gt;</source>
        <translation>&lt;b&gt;År&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="269"/>
        <source>&lt;b&gt;Label&lt;/b&gt;</source>
        <translation>&lt;b&gt;Platemerke&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="349"/>
        <source>gain &gt; </source>
        <translation></translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="409"/>
        <source>Play extract</source>
        <translation type="obsolete">Spill utdrag</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="431"/>
        <source>&amp;PRF report</source>
        <translation>PRF-rapport</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="441"/>
        <source>Open a report form for AUX production music</source>
        <translation>Åpne et rapportskjema for AUX produksjonsmusikk</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="444"/>
        <source>&amp;AUX report</source>
        <translation>AUX-rapport</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="454"/>
        <source>Apollo report</source>
        <translation>Apollo-rapport</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="464"/>
        <source>Create a list that is suitable for end credits.</source>
        <translation>Lag en liste som passer som rulletekst</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="467"/>
        <source>Credits</source>
        <translation>Rulletekst</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="512"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="pling-plong-odometer.ui" line="546"/>
        <source>About ♫ ♪ Odometer</source>
        <translation>Om ♫ ♪ Odometer</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="551"/>
        <source>Licenses</source>
        <translation>Lisenser</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="556"/>
        <source>Help</source>
        <translation>Hjelp</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="559"/>
        <source>Show a help document</source>
        <translation>Vis et hjelpedokument</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="572"/>
        <source>Logs</source>
        <translation>Logg</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="571"/>
        <source>Show recognised patterns</source>
        <translation type="obsolete">Vis gjenkjente filnavn</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="583"/>
        <source>Show recognised file name patterns</source>
        <translation>Vis gjenkjente mønster på filnavn</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="563"/>
        <source>Check for updates...</source>
        <translation type="obsolete">Sjekk etter oppdateringer</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="567"/>
        <source>Check for updates online</source>
        <translation>Sjekk etter oppdateringer på nettet</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="235"/>
        <source>&lt;b&gt;Record Number&lt;/b&gt;</source>
        <translation>&lt;b&gt;Platenummer&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="184"/>
        <source>&lt;b&gt;Lyricist&lt;/b&gt;</source>
        <translation>&lt;b&gt;Tekstforfatter&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="218"/>
        <source>&lt;b&gt;Copyright Owner&lt;/b&gt;</source>
        <translation>&lt;b&gt;Copyrightinnehaver&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="209"/>
        <source>Logs...</source>
        <translation type="obsolete">Logg...</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="493"/>
        <source>Report error</source>
        <translation>Rapporter feil</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="490"/>
        <source>Experiencing errors or glitches? Report it!</source>
        <translation>Opplever du feil eller mangler? Send det inn!</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="564"/>
        <source>Check for updates</source>
        <translation>Sjekk etter oppdateringer</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="524"/>
        <source>Settings</source>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="575"/>
        <source>Show application logs (helpful if something is wrong)</source>
        <translation>Vis programlogg (kan være til hjelp hvis noe er galt)</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="588"/>
        <source>Log on to online services</source>
        <translation>Logg inn på online tjenester</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="591"/>
        <source>Log on to services like AUX and Apollo</source>
        <translation>Logg inn på tjenester som AUX og Apollo </translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="93"/>
        <source>Check clips that you want to include in your report</source>
        <translation>Kryss av på klippene du vil ha med i rapporten</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="106"/>
        <source>Duration of the clip</source>
        <translation>Klippets varighet</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="114"/>
        <source>A summary of metadata</source>
        <translation>Sammendrag av metadata</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="580"/>
        <source>Show recognised file patterns</source>
        <translation>Vis gjenkjente filmønstre</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="535"/>
        <source>Advanced</source>
        <translation>Avansert</translation>
    </message>
    <message>
        <location filename="pling-plong-odometer.ui" line="599"/>
        <source>Export detailed report</source>
        <translation>Exportér detaljert rapport</translation>
    </message>
</context>
<context>
    <name>Odometer</name>
    <message>
        <location filename="main.py" line="783"/>
        <source>Metadata</source>
        <translation>Metadata</translation>
    </message>
    <message>
        <location filename="main.py" line="1091"/>
        <source>Credits</source>
        <translation>Rulletekst</translation>
    </message>
    <message>
        <location filename="main.py" line="213"/>
        <source>Edit</source>
        <translation>Redigér</translation>
    </message>
    <message>
        <location filename="main.py" line="216"/>
        <source>Manual lookup</source>
        <translation>Manuelt oppslag</translation>
    </message>
    <message>
        <location filename="main.py" line="244"/>
        <source>Drop your xml file here</source>
        <translation>Slipp xml-fila di her</translation>
    </message>
    <message>
        <location filename="main.py" line="285"/>
        <source>This does not seem to be a valid FCP XML file. Sorry.</source>
        <translation>Dette ser ikke ut til å være en gyldig FCP XML-fil. Beklager.</translation>
    </message>
    <message>
        <location filename="main.py" line="316"/>
        <source>No XMEML loaded</source>
        <translation>Ingen XMEML lest</translation>
    </message>
    <message>
        <location filename="main.py" line="343"/>
        <source>Unexpected error: %s</source>
        <translation>Uventet feil: %s</translation>
    </message>
    <message>
        <location filename="main.py" line="419"/>
        <source>Could not load help document, sorry. :(</source>
        <translation>Kunne ikke laste hjelp-dokumentet, beklager :(</translation>
    </message>
    <message>
        <location filename="main.py" line="427"/>
        <source>This project is free software</source>
        <translation>Dette prosjektet er fri programvare</translation>
    </message>
    <message>
        <location filename="main.py" line="428"/>
        <source>You may use and redistribute it according to the GPL license, version 3</source>
        <translation>Du kan bruke og redistribuere det i samsvar med versjon 3 av GPL-lisensen</translation>
    </message>
    <message>
        <location filename="main.py" line="447"/>
        <source>Could not look up the most recent version online. Check your internet connection</source>
        <translation>Kunne ikke slå opp siste versjon på nett. Sjekk internett-tilkoblinga di</translation>
    </message>
    <message>
        <location filename="main.py" line="457"/>
        <source>Oooooo!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main.py" line="457"/>
        <source>Odometer is out of date. 
Get the new version: %s</source>
        <translation>Odometer er utdatert. 
Hent den nye versjonen: %s</translation>
    </message>
    <message>
        <location filename="main.py" line="459"/>
        <source>Relax</source>
        <translation>Slapp av</translation>
    </message>
    <message>
        <location filename="main.py" line="459"/>
        <source>Odometer is up to date</source>
        <translation>Odometer er siste versjon</translation>
    </message>
    <message>
        <location filename="main.py" line="585"/>
        <source>Updating AUX repertoire</source>
        <translation>Oppdaterer AUX-repertoaret</translation>
    </message>
    <message>
        <location filename="main.py" line="595"/>
        <source>Found fresh AUX repertoire list in cache</source>
        <translation>Fant et nytt AUX-repertoar i cache</translation>
    </message>
    <message>
        <location filename="main.py" line="601"/>
        <source>AUX repertoire cache is too old, fetch new online</source>
        <translation>AUX-repertoaret i cachen er for gammelt, henter nytt fra nett</translation>
    </message>
    <message>
        <location filename="main.py" line="622"/>
        <source>Open an xmeml file (FCP export)</source>
        <translation>Åpne en xmeml-fil (eksportert fra FCP)</translation>
    </message>
    <message>
        <location filename="main.py" line="622"/>
        <source>Xmeml files (*.xml)</source>
        <translation>Xmeml-filer (*.xml)</translation>
    </message>
    <message>
        <location filename="main.py" line="638"/>
        <source>Loading %s...</source>
        <translation>Leser %s...</translation>
    </message>
    <message>
        <location filename="main.py" line="642"/>
        <source>&lt;b&gt;Loaded:&lt;/b&gt; %s</source>
        <translation>&lt;b&gt;Leste:&lt;/b&gt; %s</translation>
    </message>
    <message>
        <location filename="main.py" line="682"/>
        <source>&lt;i&gt;(above %i dB)&lt;/i&gt;</source>
        <translation>&lt;i&gt;(over %i dB)&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="675"/>
        <source>%i audio clips loaded from xmeml sequence u00ab%su00bb.</source>
        <translation>%i lydspor lest fra xmeml-sekvensen \u00ab%s\u00bb.</translation>
    </message>
    <message>
        <location filename="main.py" line="750"/>
        <source>Incomplete metadata. Please update manually</source>
        <translation>Ufullstendige metadata. Vennligst oppdatert manuelt</translation>
    </message>
    <message>
        <location filename="main.py" line="791"/>
        <source>&lt;i&gt;Name:&lt;/i&gt;&lt;br&gt;%(name)s&lt;br&gt;
                &lt;i&gt;Total length:&lt;/i&gt;&lt;br&gt;%(secs)ss&lt;br&gt;
                &lt;i&gt;Rate:&lt;/i&gt;&lt;br&gt;%(timebase)sfps&lt;br&gt;
                </source>
        <translation>&lt;i&gt;Navn:&lt;/i&gt;&lt;br&gt;%(name)s&lt;br&gt;
&lt;i&gt;Total lengde:&lt;/i&gt;&lt;br&gt;%(secs)ss&lt;br&gt;
&lt;i&gt;Rammerate:&lt;/i&gt;&lt;br&gt;%(timebase)sfps&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="796"/>
        <source>&lt;i&gt;Library&lt;/i&gt;&lt;br&gt;%s&lt;br&gt;</source>
        <translation>&lt;i&gt;Katalog&lt;/i&gt;&lt;br&gt;%s&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="815"/>
        <source>Unknown</source>
        <translation>Ukjent</translation>
    </message>
    <message>
        <location filename="main.py" line="700"/>
        <source>Not applicable</source>
        <translation type="obsolete">Gjelder ikke</translation>
    </message>
    <message>
        <location filename="main.py" line="701"/>
        <source>&lt;dl&gt;
            &lt;dt&gt;Title:&lt;/dt&gt;&lt;dd&gt;%(title)s&lt;/dd&gt;
            &lt;dt&gt;Artist:&lt;/dt&gt;&lt;dd&gt;%(artist)s&lt;/dd&gt;
            &lt;dt&gt;Album name:&lt;/dt&gt;&lt;dd&gt;%(albumname)s&lt;/dd&gt;
            &lt;dt&gt;Lyricist:&lt;/dt&gt;&lt;dd&gt;%(lyricist)s&lt;/dd&gt;
            &lt;dt&gt;Composer:&lt;/dt&gt;&lt;dd&gt;%(composer)s&lt;/dd&gt;
            &lt;dt&gt;Label:&lt;/dt&gt;&lt;dd&gt;%(label)s&lt;/dd&gt;
            &lt;dt&gt;Recordnumber:&lt;/dt&gt;&lt;dd&gt;%(recordnumber)s&lt;/dd&gt;
            &lt;dt&gt;Copyright owner:&lt;/dt&gt;&lt;dd&gt;%(copyright)s&lt;/dd&gt;
            &lt;dt&gt;Released year:&lt;/dt&gt;&lt;dd&gt;%(year)s&lt;/dd&gt;
            &lt;/dl&gt;</source>
        <translation type="obsolete">&lt;dl&gt;
            &lt;dt&gt;Tittel:&lt;/dt&gt;&lt;dd&gt;%(title)s&lt;/dd&gt;itle)s&lt;/dd&gt;
            &lt;dt&gt;Artist:&lt;/dt&gt;&lt;dd&gt;%(artist)s&lt;/dd&gt;tist)s&lt;/dd&gt;
            &lt;dt&gt;Album:&lt;/dt&gt;&lt;dd&gt;%(albumname)s&lt;/dd&gt;name)s&lt;/dd&gt;
            &lt;dt&gt;Tekstforfatter:&lt;/dt&gt;&lt;dd&gt;%(lyricist)s&lt;/dd&gt;cist)s&lt;/dd&gt;
            &lt;dt&gt;Komponist:&lt;/dt&gt;&lt;dd&gt;%(composer)s&lt;/dd&gt;oser)s&lt;/dd&gt;
            &lt;dt&gt;Label:&lt;/dt&gt;&lt;dd&gt;%(label)s&lt;/dd&gt;abel)s&lt;/dd&gt;
            &lt;dt&gt;Platenummer:&lt;/dt&gt;&lt;dd&gt;%(recordnumber)s&lt;/dd&gt;mber)s&lt;/dd&gt;
            &lt;dt&gt;Copyright eies av:&lt;/dt&gt;&lt;dd&gt;%(copyright)s&lt;/dd&gt;
            &lt;dt&gt;Utgitt år:&lt;/dt&gt;&lt;dd&gt;%(year)s&lt;/dd&gt;
            &lt;/dl&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="888"/>
        <source>Seconds in total&lt;/b&gt;: %s</source>
        <translation>Sekunder totalt&lt;/b&gt;: %s</translation>
    </message>
    <message>
        <location filename="main.py" line="890"/>
        <source>, in these subclips:</source>
        <translation>, i disse subklippene:</translation>
    </message>
    <message>
        <location filename="main.py" line="760"/>
        <source>Save prf report</source>
        <translation type="obsolete">Lagre prf-rapport</translation>
    </message>
    <message>
        <location filename="main.py" line="905"/>
        <source>Prf report saved</source>
        <translation>PRF-rapport lagret</translation>
    </message>
    <message>
        <location filename="main.py" line="951"/>
        <source>&quot;%s&quot; cannot be blank</source>
        <translation>&quot;%s&quot; kan ikke være blank</translation>
    </message>
    <message>
        <location filename="main.py" line="1030"/>
        <source>Music ID</source>
        <translation>Musikk-ID</translation>
    </message>
    <message>
        <location filename="main.py" line="1030"/>
        <source>Enter the correct music ID:</source>
        <translation>Skriv inn korrekt musikk-ID</translation>
    </message>
    <message>
        <location filename="main.py" line="880"/>
        <source>Save credits</source>
        <translation type="obsolete">Lagre rulletekst</translation>
    </message>
    <message>
        <location filename="main.py" line="1087"/>
        <source>End credits saved</source>
        <translation>Rulletekst lagret</translation>
    </message>
    <message>
        <location filename="main.py" line="1142"/>
        <source>Need production number</source>
        <translation>Trenger produksjonsnummer</translation>
    </message>
    <message>
        <location filename="main.py" line="1142"/>
        <source>You must enter the production number</source>
        <translation>Du må oppgi produksjonsnummeret</translation>
    </message>
    <message>
        <location filename="main.py" line="867"/>
        <source>&lt;h1&gt;Track metadata sheet for PRF&lt;/h1&gt;</source>
        <translation>&lt;h1&gt;Metadata-ark til PRF&lt;/h1&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="870"/>
        <source>&lt;div&gt;&lt;dt&gt;Title:&lt;/dt&gt;&lt;dd&gt;%s&lt;/dd&gt;</source>
        <translation>&lt;div&gt;&lt;dt&gt;Tittel:&lt;/dt&gt;&lt;dd&gt;%s&lt;/dd&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="872"/>
        <source>&lt;dt&gt;Artist:&lt;/dt&gt;&lt;dd&gt;%s&lt;/dd&gt;</source>
        <translation>&lt;div&gt;&lt;dt&gt;Artist:&lt;/dt&gt;&lt;dd&gt;%s&lt;/dd&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="874"/>
        <source>&lt;dt&gt;Album name:&lt;/dt&gt;&lt;dd&gt;%s&lt;/dd&gt;</source>
        <translation>&lt;div&gt;&lt;dt&gt;Albumnavn:&lt;/dt&gt;&lt;dd&gt;%s&lt;/dd&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="876"/>
        <source>&lt;dt&gt;Lyricist:&lt;/dt&gt;&lt;dd&gt;%s&lt;/dd&gt;</source>
        <translation>&lt;div&gt;&lt;dt&gt;Tekstforfatter:&lt;/dt&gt;&lt;dd&gt;%s&lt;/dd&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="878"/>
        <source>&lt;dt&gt;Composer:&lt;/dt&gt;&lt;dd&gt;%s&lt;/dd&gt;</source>
        <translation>&lt;div&gt;&lt;dt&gt;Komponist:&lt;/dt&gt;&lt;dd&gt;%s&lt;/dd&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="880"/>
        <source>&lt;dt&gt;Label:&lt;/dt&gt;&lt;dd&gt;%s&lt;/dd&gt;</source>
        <translation>&lt;div&gt;&lt;dt&gt;Platemerke:&lt;/dt&gt;&lt;dd&gt;%s&lt;/dd&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="882"/>
        <source>&lt;dt&gt;Recordnumber:&lt;/dt&gt;&lt;dd&gt;%s&lt;/dd&gt;</source>
        <translation>&lt;div&gt;&lt;dt&gt;Platenummer:&lt;/dt&gt;&lt;dd&gt;%s&lt;/dd&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="884"/>
        <source>&lt;dt&gt;Copyright owner:&lt;/dt&gt;&lt;dd&gt;%s&lt;/dd&gt;</source>
        <translation>&lt;div&gt;&lt;dt&gt;Hvem har rettighetene:&lt;/dt&gt;&lt;dd&gt;%s&lt;/dd&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="886"/>
        <source>&lt;dt&gt;Released year:&lt;/dt&gt;&lt;dd&gt;%s&lt;/dd&gt;</source>
        <translation>&lt;div&gt;&lt;dt&gt;Utgitt år:&lt;/dt&gt;&lt;dd&gt;%s&lt;/dd&gt;</translation>
    </message>
    <message>
        <location filename="main.py" line="899"/>
        <source>Save PRF report (as HTML)</source>
        <translation>Lagre prf-rapporten (som HTML)</translation>
    </message>
    <message>
        <location filename="main.py" line="1083"/>
        <source>HTML document (*.html)</source>
        <translation>HTML-dokument (*.html)</translation>
    </message>
    <message>
        <location filename="main.py" line="1083"/>
        <source>Save credits (as HTML)</source>
        <translation>Lagre rulletekst (som HTML)</translation>
    </message>
    <message>
        <location filename="main.py" line="709"/>
        <source>Track from Apollo Music detected. Please log in to the service</source>
        <translation>Et spor fra Apollo Music er gjenkjent. Vennligst logg inn på tjenesten</translation>
    </message>
    <message>
        <location filename="main.py" line="710"/>
        <source>No logincookie from apollo music found.</source>
        <translation>Finner ingen logincookie fra Apollo Music.</translation>
    </message>
    <message>
        <location filename="main.py" line="610"/>
        <source>AUX repertoire: %s catalogs</source>
        <translation>AUX-repertoar: %s kataloger</translation>
    </message>
    <message>
        <location filename="main.py" line="688"/>
        <source>Skipping clip &quot;%s&quot; because no frames are audible</source>
        <translation>Hopper over klippet &quot;%s&quot;, fordi ingen rammer (frames) er hørbare</translation>
    </message>
</context>
<context>
    <name>PlingPlongAUXDialog</name>
    <message>
        <location filename="pling-plong-auxreport.ui" line="17"/>
        <source>Sonoton (AUX) Report</source>
        <translation>Sonoton (AUX)-rapport</translation>
    </message>
    <message>
        <location filename="pling-plong-auxreport.ui" line="20"/>
        <source>This is an embedded view of the online report form for AUX publishing</source>
        <translation>Dette er en nettrapport for AUX Publishing</translation>
    </message>
    <message>
        <location filename="pling-plong-auxreport.ui" line="30"/>
        <source>about:blank</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PlingPlongPRFDialog</name>
    <message>
        <location filename="pling-plong-prfreport.ui" line="17"/>
        <source>PRF Report</source>
        <translation>PRF-rapport</translation>
    </message>
    <message>
        <location filename="pling-plong-prfreport.ui" line="20"/>
        <source>Report all music to PRF</source>
        <translation>Rapportér all musikk til PRF</translation>
    </message>
</context>
</TS>
